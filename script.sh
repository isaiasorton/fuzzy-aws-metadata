#!/bin/bash

# Base endpoint
BASE_URL="http://169.254.169.254/latest/meta-data/"

# Paths to fuzzing
PATHS=(
    ami-id
    ami-launch-index
    ami-manifest-path
    block-device-mapping/
    events/
    hostname
    iam/
    identity-credentials/
    instance-action
    instance-id
    instance-life-cycle
    instance-type
    local-hostname
    local-ipv4
    mac
    metrics/
    network/
    placement/
    profile
    public-hostname
    public-ipv4
    reservation-id
    security-groups
    services/
    system
)

for path in "${PATHS[@]}"; do
    echo "Testando: $BASE_URL$path"
    # request and response
    curl -s "${BASE_URL}${path}"
    echo -e "\n"
done
